# Copyright 2019 Emmanuel Madrigal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Based on:
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/speech_commands/input_data.py
"""
Data loader for a series of wav files with given labels
"""
import glob
import logging
import math
import os
import os.path
import random
import tarfile
import sys

from six.moves import urllib

import numpy as np

import librosa
import librosa.display

from variables import VALIDATION_PERCENTAGE, TESTING_PERCENTAGE, SILENCE_PERCENTAGE, UNKNOWN_PERCENTAGE

SILENCE_LABEL = '_silence_'
SILENCE_INDEX = 0
UNKNOWN_WORD_LABEL = '_unknown_'
UNKNOWN_WORD_INDEX = 1
BACKGROUND_NOISE_DIR_NAME = '_background_noise_'
RANDOM_SEED = 59185

DESIRED_SAMPLES = 16000

N_FFT = int(25 * DESIRED_SAMPLES / 1000)  # 25ms
HOP_LENGHT = int(10 * DESIRED_SAMPLES / 1000)  # 10ms
N_MELS = 40
FINGERPRINT_WIDTH = 2**math.ceil(math.log(DESIRED_SAMPLES /
                                          HOP_LENGHT, 2))
FINGERPRINT_WIDTH = math.ceil(DESIRED_SAMPLES / HOP_LENGHT) + 1

# Normalization parametersm determined heuristically
MAX_LOG = 7
MIN_LOG = -10


def prepare_words_list(wanted_words):
    """Prepends common tokens to the custom word list.

    Args:
      wanted_words: List of strings containing the custom words.

    Returns:
      List with the standard silence and unknown tokens added.

    """
    return [SILENCE_LABEL, UNKNOWN_WORD_LABEL] + wanted_words


def which_set(validation_percentage, testing_percentage):
    """Determines which data partition the file should belong to.

    Args:
      filename: File path of the data sample.

    Returns:
      String, one of 'training', 'validation', or 'testing'.
    """
    percentage_rand = random.randint(1, 100)
    if percentage_rand < validation_percentage:
        result = 'validation'
    elif percentage_rand < (testing_percentage + validation_percentage):
        result = 'testing'
    else:
        result = 'training'
    return result


def stretch(data, rate=1):
    """
    Function to receive a numpy array and return a speed-up or slowed down
    audio of the same size
    """
    input_length = len(data)
    data = librosa.effects.time_stretch(data, rate)
    if len(data) > input_length:
        data = data[:input_length]
    else:
        data = np.pad(data, (0, max(0, input_length - len(data))), "constant")

    return data


def processing_graph(
        wav_filename,
        augmentation_arguments):
    """
    Builds a TensorFlow graph to apply the input distortions.

    Loads a WAVE file, decodes it, scales the volume,
    shifts it in time, adds in background noise, calculates a spectrogram, and
    then builds an MFCC fingerprint from that..
    """
    wav_data, sample_rate = librosa.load(wav_filename, sr=DESIRED_SAMPLES)

    if wav_data.size >= DESIRED_SAMPLES:
        wav_data = wav_data[:DESIRED_SAMPLES]
        wav_data.shape = (DESIRED_SAMPLES)  # Doing this avoids a memory copy
    else:
        logging.warning("Loaded sample: %s is too short", wav_filename)
        return None

    # Allow the audio sample's volume to be adjusted.
    scaled_foreground = augmentation_arguments["foreground_volume"] * wav_data

    # Shift the sample's start position
    timeshift_samples = int(
        augmentation_arguments["timeshift_amount"] *
        DESIRED_SAMPLES /
        1000)
    rolled_foreground = np.roll(scaled_foreground, timeshift_samples)

    # Make the sound deeper or higher
    if augmentation_arguments["stretch_amount"] > 0:
        stretched_foreground = stretch(
            rolled_foreground,
            augmentation_arguments["stretch_amount"])
    else:
        stretched_foreground = rolled_foreground

    # Mix in background noise.
    background_mul = stretched_foreground + \
        augmentation_arguments["background_volume"] * \
        augmentation_arguments["background_data"]

    # Run the spectrogram and MFCC ops to get a 2D 'fingerprint' of
    # the audio.
    mel_spectogram = librosa.feature.melspectrogram(
        background_mul,
        sr=sample_rate,
        n_fft=N_FFT,
        hop_length=HOP_LENGHT,
        n_mels=N_MELS)

    mel_spectogram = np.log10(mel_spectogram + 1e-10)

    # Normalize
    normalized_mel = (mel_spectogram - MIN_LOG) / (MAX_LOG - MIN_LOG)

    return normalized_mel


def maybe_download_and_extract_dataset(data_url, dest_directory):
    """Download and extract data set tar file.

    If the data set we're using doesn't already exist, this function
    downloads it from the TensorFlow.org website and unpacks it into a
    directory.
    If the data_url is none, don't download anything and expect the data
    directory to contain the correct files already.

    Args:
      data_url: Web location of the tar file containing the data set.
      dest_directory: File path to extract data to.
    """
    if not data_url:
        return

    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)

    filename = data_url.split('/')[-1]
    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write(
                '\r>> Downloading %s %.1f%%' %
                (filename,
                 float(
                     count *
                     block_size) /
                 float(total_size) *
                 100.0))
            sys.stdout.flush()

        try:
            filepath, _ = urllib.request.urlretrieve(
                data_url, filepath, _progress)
        except BaseException:
            logging.error(
                'Failed to download URL: %s to folder: %s',
                data_url,
                filepath)
            logging.error(
                'Please make sure you have enough free space and'
                ' an internet connection')
            raise
        statinfo = os.stat(filepath)
        logging.info('Successfully downloaded %s (%d bytes)',
                     filename, statinfo.st_size)
        tarfile.open(filepath, 'r:gz').extractall(dest_directory)


class AudioProcessor():
    """Handles loading, partitioning, and preparing audio training data."""

    def __init__(
            self,
            data_url,
            data_dir,
            wanted_words):
        self._validation_percentage = VALIDATION_PERCENTAGE
        self._testing_percentage = TESTING_PERCENTAGE
        self._silence_percentage = SILENCE_PERCENTAGE
        self._unknown_percentage = UNKNOWN_PERCENTAGE

        if data_dir:
            self.data_dir = data_dir
            maybe_download_and_extract_dataset(data_url, data_dir)

        self.prepare_background_data()
        self.prepare_data_index(wanted_words)

    def prepare_data_index(self, wanted_words):
        """Prepares a list of the samples organized by set and label.

        The training loop needs a list of all the available data,
        organized by which partition it should belong to, and with
        ground truth labels attached.  This function analyzes the
        folders below the `data_dir`, figures out the right labels for
        each file based on the name of the subdirectory it belongs to,
        and uses a stable hash to assign it to a data set partition.

        Args:
          wanted_words: Labels of the classes we want to be able to
          recognize.

        Returns:
          Dictionary containing a list of file information for each
          set partition, and a lookup map for each class to determine
          its numeric index.

        Raises:
          Exception: If expected files are not found.

        """
        # Make sure the shuffling and picking of unknowns is
        # deterministic.
        random.seed(RANDOM_SEED)
        wanted_words_index = {}
        for index, wanted_word in enumerate(wanted_words):
            wanted_words_index[wanted_word] = index + 2

        self.data_index = {'validation': [], 'testing': [], 'training': []}
        unknown_index = {'validation': [], 'testing': [], 'training': []}
        all_words = {}
        # Look through all the subfolders to find audio samples
        search_path = os.path.join(self.data_dir, '*', '*.wav')
        for wav_path in glob.glob(search_path):
            _, word = os.path.split(os.path.dirname(wav_path))
            word = word.lower()
            # Treat the '_background_noise_' folder as a special case,
            # since we expect it to contain long audio samples we mix
            # in to improve training.
            if word == BACKGROUND_NOISE_DIR_NAME:
                continue

            all_words[word] = True
            set_index = which_set(
                self._validation_percentage,
                self._testing_percentage)
            # If it's a known class, store its detail, otherwise add
            # it to the list we'll use to train the unknown label.
            if word in wanted_words_index:
                self.data_index[set_index].append(
                    {'label': word, 'file': wav_path})
            else:
                unknown_index[set_index].append(
                    {'label': word, 'file': wav_path})

        if not all_words:
            raise Exception('No .wavs found at ' + search_path)

        for index, wanted_word in enumerate(wanted_words):
            if wanted_word not in all_words:
                raise Exception('Expected to find ' + wanted_word +
                                ' in labels but only found ' +
                                ', '.join(all_words.keys()))

        # We need an arbitrary file to load as the input for the
        # silence samples.  It's multiplied by zero later, so the
        # content doesn't matter.
        silence_wav_path = self.data_index['training'][0]['file']
        for set_index in ['validation', 'testing', 'training']:
            set_size = len(self.data_index[set_index])
            silence_size = int(
                math.ceil(
                    set_size *
                    self._silence_percentage /
                    100))
            for _ in range(silence_size):
                self.data_index[set_index].append({
                    'label': SILENCE_LABEL,
                    'file': silence_wav_path
                })

            # Pick some unknowns to add to each partition of the data set.
            random.shuffle(unknown_index[set_index])
            unknown_size = int(
                math.ceil(
                    set_size *
                    self._unknown_percentage /
                    100))
            self.data_index[set_index].extend(
                unknown_index[set_index][:unknown_size])

        # Make sure the ordering is random.
        for set_index in ['validation', 'testing', 'training']:
            random.shuffle(self.data_index[set_index])

        # Prepare the rest of the result data structure.
        self.words_list = prepare_words_list(wanted_words)
        self.word_to_index = {}
        for word in all_words:
            if word in wanted_words_index:
                self.word_to_index[word] = wanted_words_index[word]
            else:
                self.word_to_index[word] = UNKNOWN_WORD_INDEX
        self.word_to_index[SILENCE_LABEL] = SILENCE_INDEX

    def prepare_background_data(self):
        """Searches a folder for background noise audio, and loads it into
        memory.

        It's expected that the background audio samples will be in a
        subdirectory named '_background_noise_' inside the 'data_dir'
        folder, as .wavs that match the sample rate of the training
        data, but can be much longer in duration.

        If the '_background_noise_' folder doesn't exist at all, this
        isn't an error, it's just taken to mean that no background
        noise augmentation should be used. If the folder does exist,
        but it's empty, that's treated as an error.

        Returns:
          List of raw PCM-encoded audio samples of background noise.

        Raises:
          Exception: If files aren't found in the folder.

        """
        self.background_data = []
        background_dir = os.path.join(self.data_dir, BACKGROUND_NOISE_DIR_NAME)
        if not os.path.exists(background_dir):
            return self.background_data

        search_path = os.path.join(
            self.data_dir, BACKGROUND_NOISE_DIR_NAME, '*.wav')
        logging.info("Loading background data")

        for wav_path in glob.glob(search_path):
            wav_data, sample_rate = librosa.load(wav_path)
            self.background_data.append(wav_data)

        if not self.background_data:
            raise Exception(
                'No background wav files were found in ' +
                search_path)

        logging.info("Done loading background data")
        return self.background_data

    def get_size(self, mode):
        """Calculates the number of samples in the dataset partition.

        Args:
          mode: Which partition, must be 'training', 'validation', or
          'testing'.

        Returns:
          Number of samples in the partition.

        """
        return len(self.data_index[mode])

    def get_data(self,
                 how_many,
                 mode,
                 background_frequency=0,
                 background_volume_range=0,
                 time_shift=0,
                 time_stretch=0
                 ):
        """Gather samples from the data set, applying transformations as
        needed.

        When the mode is 'training', a random selection of samples
        will be returned, otherwise the first N clips in the partition
        will be used. This ensures that validation always uses the
        same samples, reducing noise in the metrics.

        Args:
            how_many: Desired number of samples to return. -1 means
                the entire contents of this partition.
            background_frequency: How many clips will have background
                noise, 0.0 to 1.0.
            background_volume_range: How loud the background noise
            will be.
            time_shift: How much to randomly shift the clips by in ms.
            time_stretch: How much to increase or reduce the audio
            speed.
            mode: Which partition to use, must be 'training',
                'validation', or 'testing'.

        Returns:
            List of sample data for the transformed samples, and list of label indexes

        Raises:
            ValueError: If background samples are too short.

        """
        # Pick one of the partitions to choose samples from.
        candidates = self.data_index[mode]
        if how_many == -1:
            sample_count = len(candidates)
        else:
            sample_count = max(0, min(how_many, len(candidates)))

        # Data and labels will be populated and returned.
        data = np.zeros((sample_count, N_MELS, FINGERPRINT_WIDTH, 1))
        labels = np.zeros(sample_count)
        desired_samples = DESIRED_SAMPLES
        use_background = (len(self.background_data) >
                          0) and (mode == 'training')
        pick_deterministically = (mode != 'training')

        # Process each sample
        for i in range(0, sample_count):
            # Pick which audio sample to use.
            if how_many == -1 or pick_deterministically:
                sample_index = i
            else:
                sample_index = np.random.randint(len(candidates))
            sample = candidates[sample_index]

            # If we're time shifting, set up the offset for this sample.
            if time_shift > 0:
                timeshift_amount = np.random.randint(-time_shift, time_shift)
            else:
                timeshift_amount = 0

            # If we're time stretching
            if time_stretch > 0:
                stretch_amount = np.random.uniform(
                    1 - time_stretch, 1 + time_stretch)
            else:
                stretch_amount = 0

            wav_filename = sample['file']

            # Choose a section of background noise to mix in.
            if use_background or (sample['label'] == SILENCE_LABEL):
                background_index = int(
                    np.random.randint(len(self.background_data)))
                background_samples = self.background_data[background_index]
                if len(background_samples) <= DESIRED_SAMPLES:
                    raise ValueError(
                        'Background sample is too short! Need more than %d'
                        ' samples but only %d were found' %
                        (DESIRED_SAMPLES, len(background_samples)))

                background_offset = np.random.randint(
                    0, len(background_samples) - DESIRED_SAMPLES)
                background_data = background_samples[background_offset:(
                    background_offset + desired_samples)]
                background_data.shape = (desired_samples)

                if sample['label'] == SILENCE_LABEL:
                    background_volume = np.random.uniform(0, 1)
                elif np.random.uniform(0, 1) < background_frequency:
                    background_volume = np.random.uniform(
                        0, background_volume_range)
                else:
                    background_volume = 0
            else:
                background_data = np.zeros((desired_samples))
                background_volume = 0

            # If we want silence, mute out the main sample but leave the
            # background.
            if sample['label'] == SILENCE_LABEL:
                foreground_volume = 0
            else:
                foreground_volume = 1

            augmentation_arguments = {
                "foreground_volume": foreground_volume,
                "timeshift_amount": timeshift_amount,
                "stretch_amount": stretch_amount,
                "background_data": background_data,
                "background_volume": background_volume
            }

            # Process the data to get the spectogram.
            data_tensor = processing_graph(
                wav_filename,
                augmentation_arguments)
            if data_tensor is not None:
                data_tensor.shape = (N_MELS, FINGERPRINT_WIDTH, 1)
                data[i, :] = data_tensor
                label_index = self.word_to_index[sample['label']]
                labels[i] = label_index
        return data, labels

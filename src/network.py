#
# Copyright 2019 Emmanuel Madrigal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This module declares a function to get the nework arquitecture
"""

import tensorflow as tf


def create_model(input_shape, output_categories):
    """
    This function return a keras neural network arquitecture
    """

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.BatchNormalization(input_shape=input_shape))
    model.add(
        tf.keras.layers.Conv2D(
            16, (5, 5), padding='same', activation='relu', strides=(2, 2)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(
        tf.keras.layers.Conv2D(
            32, (5, 5), padding='same', activation='relu', strides=(2, 2)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(
        tf.keras.layers.Conv2D(
            32, (5, 5), padding='same', activation='relu', strides=(2, 2)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(
        tf.keras.layers.Conv2D(
            16, (5, 5), padding='same', activation='relu', strides=(2, 2)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(
        tf.keras.layers.Conv2D(
            8, (5, 5), padding='same', activation='relu', strides=(1, 1)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(
        tf.keras.layers.Conv2D(
            4, (5, 5), padding='same', activation='relu', strides=(1, 1)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(
        tf.keras.layers.Conv2D(
            2, (5, 5), padding='same', activation='relu', strides=(1, 1)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(output_categories))
    model.add(tf.keras.layers.Activation('softmax'))
    model.summary()
    return model

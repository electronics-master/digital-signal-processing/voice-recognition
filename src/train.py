# Copyright 2019 Emmanuel Madrigal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Runs the training for this dataset
"""
import os
import pickle

import numpy as np
import tensorflow as tf

from network import create_model
from dataset import AudioProcessor
from variables import DATA_URL, DATA_DIR, LABELS, PREPROCESS_DATA_FILENAME


def scheduler(epoch):
    """This function keeps the learning rate at 0.001 for the first ten
    epochs and then decreases it exponetially
    """
    learning_rate = 0.001
    if epoch > 20:
        learning_rate = float(0.001 * 2 ** ((20 - epoch) / 5))

    return learning_rate


def train():
    """
    Runs the training
    """

    audio_processor = AudioProcessor(
        DATA_URL,
        DATA_DIR,
        LABELS)

    # Without this there is a cuDNN error when loading conv2D layers
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
                logical_gpus = tf.config.experimental.list_logical_devices(
                    'GPU')
                print(
                    len(gpus),
                    "Physical GPUs,",
                    len(logical_gpus),
                    "Logical GPUs")
        except RuntimeError as exception:
            # Memory growth must be set before GPUs have been initialized
            print(exception)

    if not os.path.isfile(PREPROCESS_DATA_FILENAME):
        train_fingerprints, train_ground_truth = audio_processor.get_data(
            -1, 'training', 0.1, 300, 200, 0.2)
        validate_fingerprints, validate_ground_truth = audio_processor.get_data(
            -1, 'validation')
        test_fingerprints, test_ground_truth = audio_processor.get_data(
            -1, 'testing')

        complete_images = [
            train_fingerprints,
            train_ground_truth,
            validate_fingerprints,
            validate_ground_truth,
            test_fingerprints,
            test_ground_truth]

        with open(PREPROCESS_DATA_FILENAME, 'wb') as filehandle:
            # store the data as binary data stream
            pickle.dump(complete_images, filehandle)
    else:
        with open(PREPROCESS_DATA_FILENAME, 'rb') as filehandle:
            complete_images = pickle.load(filehandle)
            train_fingerprints = complete_images[0]
            train_ground_truth = complete_images[1]
            validate_fingerprints = complete_images[2]
            validate_ground_truth = complete_images[3]
            test_fingerprints = complete_images[4]
            test_ground_truth = complete_images[5]

    # Include the epoch in the file name (uses `str.format`)
    checkpoint_path = "training_checkpoints/cp-{epoch:04d}.ckpt"

    # Create a callback that saves the model's weights every 5 epochs
    cp_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_path,
        verbose=1,
        save_weights_only=True
    )

    sch_callback = tf.keras.callbacks.LearningRateScheduler(scheduler)

    tb_callback = tf.keras.callbacks.TensorBoard(log_dir='./Graph', histogram_freq=0,
          write_graph=True, write_images=True)

    input_shape = list(train_fingerprints[0].shape)
    # The two is for silence and unknown labels
    model = create_model(input_shape, len(LABELS) + 2)

    # Save the weights using the `checkpoint_path` format
    model.save_weights(checkpoint_path.format(epoch=0))

    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3, ),
        loss='sparse_categorical_crossentropy',
        metrics=['sparse_categorical_accuracy']
    )

    history = model.fit(
        train_fingerprints.astype(
            np.float32), train_ground_truth.astype(
            np.float32), batch_size=256, validation_data=(
                validate_fingerprints.astype(
                    np.float32), validate_ground_truth.astype(
                        np.float32)), epochs=100, callbacks=[
                            cp_callback, sch_callback, tb_callback])

    print('\nhistory dict:', history.history)

    # Evaluate the model on the test data using `evaluate`
    print('\n# Evaluate on test data')
    results = model.evaluate(
        test_fingerprints,
        test_ground_truth,
        batch_size=128)
    print('test loss, test acc:', results)

    # Generate predictions (probabilities -- the output of the last layer)
    # on new data using `predict`
    print('\n# Generate predictions for 3 samples')
    predictions = model.predict(test_fingerprints[:3])

    print("Predictions:\n", predictions)

# Copyright 2019 Emmanuel Madrigal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This module runs the validation on a model
"""

import os
import tensorflow as tf
import pickle

from network import create_model
from dataset import AudioProcessor
from variables import DATA_URL, DATA_DIR, LABELS, PREPROCESS_DATA_FILENAME

tf.enable_eager_execution()

def test(model_filename):
    """
    This function runs the validation on a given model
    """

    audio_processor = AudioProcessor(
        DATA_URL,
        DATA_DIR,
        LABELS)

    # Without this there is a cuDNN error when loading conv2D layers
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
                logical_gpus = tf.config.experimental.list_logical_devices(
                    'GPU')
                print(
                    len(gpus),
                    "Physical GPUs,",
                    len(logical_gpus),
                    "Logical GPUs")
        except RuntimeError as exception:
            # Memory growth must be set before GPUs have been initialized
            print(exception)

    if not os.path.isfile(PREPROCESS_DATA_FILENAME):
        test_fingerprints, test_ground_truth = audio_processor.get_data(
            -1, 'testing')
    else:
        with open(PREPROCESS_DATA_FILENAME, 'rb') as filehandle:
            complete_images = pickle.load(filehandle)
            test_fingerprints = complete_images[4]
            test_ground_truth = complete_images[5]

    input_shape = list(test_fingerprints[0].shape)
    # The two is for silence and unknown labels
    model = create_model(input_shape, len(LABELS) + 2)

    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3, ),
        loss='sparse_categorical_crossentropy',
        metrics=['sparse_categorical_accuracy']
    )

    model.load_weights(model_filename)

    # Evaluate the model on the test data using `evaluate`
    print('\n# Evaluate on test data')
    results = model.evaluate(
        test_fingerprints,
        test_ground_truth,
        batch_size=128)
    print('test loss, test acc:', results)

    # Generate predictions (probabilities -- the output of the last layer)
    # on new data using `predict`
    print('\n# Generate predictions for 3 samples')
    predictions = model.predict(test_fingerprints)
    predictions_index = [r.argmax(axis=0) for r in predictions]

    confusion = tf.math.confusion_matrix(
        predictions_index, test_ground_truth).numpy()
    print(confusion)

#!/usr/bin/env python3

# Copyright 2019 Emmanuel Madrigal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This is the main module, it will read commands from the user and
call the appropiate command
"""

import argparse
import sys
import logging

from test import test
from train import train


def cmdline_args():
    """
    This function declares the commands that will be parsed
    from the user
    """
    # Make parser object
    args_parser = argparse.ArgumentParser(
        description="""
        Voice commands recognition

        This program recognizes one of ten commands:
        * yes
        * no
        * up
        * down
        * left
        * right
        * on
        * off
        * stop
        * go
        * unknown (other commands)
        * background noise/silence
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    args_parser.add_argument("--model", type=str,
                             help="Location of the model to use")

    group1 = args_parser.add_mutually_exclusive_group(required=True)
    group1.add_argument('--train', action="store_true")
    group1.add_argument('--test', action="store_true")
    group1.add_argument('--real_time', action="store_true")

    return args_parser


if __name__ == '__main__':

    if sys.version_info < (3, 0, 0):
        sys.stderr.write("You need python 3.0 or later to run this script\n")
        sys.exit(1)

    PARAMS = cmdline_args()

    ARGS = PARAMS.parse_args()

    if ARGS.train:
        train()
    elif ARGS.test:
        if ARGS.model:
            test(ARGS.model)
        else:
            logging.error("A model is needed for testing")
    elif ARGS.real_time:
        logging.error("Real time hasn't been implemented")
